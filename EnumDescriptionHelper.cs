#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Helper class for enum description attributes.
    /// </summary>
    public static class EnumDescriptionHelper
    {
        /// <summary>
        /// Gets the description of the enum value from its attribute.
        /// </summary>
        /// <returns>The description.</returns>
        /// <param name="value">Enum value.</param>
        /// <typeparam name="T">Enum type parameter.</typeparam>
        public static string GetDescription<T>(T value)
        {
            Type type = value.GetType();
            MemberInfo[] memberInfo = type.GetMember(value.ToString());
            if (memberInfo.Length == 0) {
                throw new Exception(String.Format("No member with the required value ({0}) found on type {1}", value.ToString(), type));
            }
            DescriptionAttribute[] attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (attributes.Length == 0) {
                throw new Exception(String.Format("The {0} member of the {1} enum has no Description attribute", value.ToString(), type));
            }
            return attributes[0].Description;
        }

        /// <summary>
        /// Builds the dictionary containing a mapping of the description to its value.
        /// </summary>
        /// <returns>The dictionary.</returns>
        /// <typeparam name="T">The enum type parameter.</typeparam>
        public static IDictionary<string, T> BuildDictionary<T>()
        {
            Dictionary<string, T> dictionary = new Dictionary<string, T>();
            T[] values = Enum.GetValues (typeof(T)) as T[];
            foreach (T value in values)
            {
                dictionary.Add (GetDescription<T>(value), value);
            }
            return dictionary;
        }
    }
}
#endif