/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Attribute for specifing the object type that should be instanciated when the associated enum value
    /// is parsed for the type property.
    /// </summary>
	public class TypeAttribute: Attribute
	{
		private Type type;

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeAttribute"/> class.
        /// </summary>
        /// <param name="type">Type.</param>
		public TypeAttribute (Type type)
		{
			this.type = type;
		}

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
		public Type Type {
			get {
				return type;
			}
		}
	}
}

