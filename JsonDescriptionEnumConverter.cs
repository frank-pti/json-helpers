#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Convert for enum descriptions
    /// </summary>
    public class JsonDescriptionEnumConverter<T>: JsonConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Json.JsonDescriptionEnumConverter`1"/> class.
        /// </summary>
        public JsonDescriptionEnumConverter ()
        {
            IgnoreCase = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Frank.Helpers.Json.JsonDescriptionEnumConverter`1"/>
        /// ignore case.
        /// </summary>
        /// <value><c>true</c> if ignore case; otherwise, <c>false</c>.</value>
        public bool IgnoreCase {
            get;
            set;
        }

#region implemented abstract members of Newtonsoft.Json.JsonConverter
        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(EnumDescriptionHelper.GetDescription<T>((T)value));
        }

        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            T[] values = Enum.GetValues (typeof(T)) as T[];
            string parsedEntry = (reader.Value as string);
            foreach (T value in values)
            {
                string descriptionEntry = EnumDescriptionHelper.GetDescription<T>(value);
                if (IgnoreCase) {
                    descriptionEntry = descriptionEntry.ToLower();
                    parsedEntry = parsedEntry.ToLower();
                }
                if (descriptionEntry == parsedEntry) {
                    return value;
                }
            }
            foreach (T value in values) {
                string nameEntry = Enum.GetName(typeof(T), value);
                if (IgnoreCase) {
                    nameEntry = nameEntry.ToLower ();
                    parsedEntry = parsedEntry.ToLower();
                }
                if (nameEntry == parsedEntry) {
                    return value;
                }
            }
            throw new Exception(String.Format ("Enum type {0} has no member with a description \"{1}\"", typeof(T).Name, reader.Value));
        }

        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(T) && objectType.IsEnum;
        }
        #endregion
    }
}
#endif
