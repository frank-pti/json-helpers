#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Base class for JSON creation converters.
    /// </summary>
    public abstract class JsonCreationConverter<T>: JsonConverter
    {
        /// <summary>
        /// Create an instance of the specified objectType from jObject.
        /// </summary>
        /// <param name="objectType">Object type.</param>
        /// <param name="jObject">Json object.</param>
        protected abstract T Create(Type objectType, JObject jObject);

        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return typeof(T).IsAssignableFrom(objectType);
        }

        /// <inheritdoc/>
        public override object ReadJson(
            JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            T target = Create (objectType, jObject);
            serializer.Populate (jObject.CreateReader(), target);
            return target;
        }

        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
#endif