#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// JSON converter for dictionaries that use the values of an enum as keys.
    /// </summary>
    public class DictionaryWithDescriptionEnumKeyConverter<TKeyEnum, TValue>: JsonConverter
        where TKeyEnum: struct
    {
        /// <inheritdoc/>
        public override bool CanWrite {
            get {
                return true;
            }
        }

        /// <inheritdoc/>
        public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
        {
            Dictionary<string, TValue> intermediateDictionary = new Dictionary<string, TValue> ();
            IDictionary<TKeyEnum, TValue> dictionary = (IDictionary<TKeyEnum, TValue>)value;
            foreach (TKeyEnum key in dictionary.Keys) {
                intermediateDictionary.Add (EnumDescriptionHelper.GetDescription(key), dictionary[key]);
            }
            serializer.Serialize (writer, intermediateDictionary);
        }

        /// <inheritdoc/>
        public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) {
                return null;
            }

            IDictionary<string, TKeyEnum> mapping = EnumDescriptionHelper.BuildDictionary<TKeyEnum> ();

            Dictionary<string, TValue> intermediateDictionary = new Dictionary<string, TValue> ();
            serializer.Populate (reader, intermediateDictionary);

            Dictionary<TKeyEnum, TValue> result = new Dictionary<TKeyEnum, TValue> ();
            foreach (string key in intermediateDictionary.Keys) {
                result.Add (mapping[key], intermediateDictionary[key]);
            }

            return result;
        }

        /// <inheritdoc/>
        public override bool CanConvert (Type objectType)
        {
            return typeof(IDictionary<TKeyEnum, TValue>).IsAssignableFrom (objectType);
        }
    }
}
#endif
