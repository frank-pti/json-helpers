#if !MICRO_FRAMEWORK
using Logging;
using Newtonsoft.Json;
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Reflection;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Serializer.
    /// </summary>
    public static class Serializer
    {
        /// <summary>
        /// Deserializes the file.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="fileName">File name.</param>
        /// <param name="createDefaultIfMissing">If set to <c>true</c> create default if missing.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeFile<T>(string fileName, bool createDefaultIfMissing)
        {
            return DeserializeFile<T>(fileName, createDefaultIfMissing, new JsonSerializerSettings());
        }

        /// <summary>
        /// Deserializes the file.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="fileName">File name.</param>
        /// <param name="createDefaultIfMissing">If set to <c>true</c> create default if missing.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeFile<T>(
            string fileName, bool createDefaultIfMissing, JsonSerializerSettings serializerSettings)
        {
            return (T)DeserializeFile(fileName, typeof(T), createDefaultIfMissing, serializerSettings);
        }

        private static object DeserializeFile(
            string fileName, Type targetType, bool createDefaultIfMissing, JsonSerializerSettings serializerSettings)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (!fileInfo.Exists) {
                Logger.Warning("Not loading file '{0}' because it does not exist.", fileInfo.FullName);
                if (createDefaultIfMissing) {
                    return Activator.CreateInstance(targetType);
                } else {
                    throw new FileNotFoundException(
                        String.Format("Cannot load file '{0}' because it does not exist.", fileInfo.FullName),
                        fileInfo.FullName);
                }
            } else if (fileInfo.Length == 0) {
                Logger.Warning("Not loading file '{0}' because it is empty.", fileInfo.FullName);
                if (createDefaultIfMissing) {
                    return Activator.CreateInstance(targetType);
                } else {
                    throw new FileLoadException(
                        String.Format("Cannot load file '{0} because it is empty.", fileInfo.FullName),
                        fileInfo.FullName);
                }
            } else {
                FileStream readStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                TextReader textReader = new StreamReader(readStream);
                return DeserializeStream(textReader, targetType, serializerSettings);
            }
        }

        /// <summary>
        /// Deserializes from assembly resource.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="assembly">The assembly containing the resource.</param>
        /// <param name="resourceName">Resource name.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeFromAssembly<T>(Assembly assembly, string resourceName)
        {
            Stream readStream = assembly.GetManifestResourceStream(resourceName);
            if (readStream == null) {
                throw new ArgumentException(String.Format("Resource \"{0}\" not found in assembly {1}", resourceName, assembly.FullName));
            }

            try {
                return DeserializeStream<T>(readStream);
            } catch (Exception e) {
                throw e;
            } finally {
                readStream.Close();
            }
        }

        /// <summary>
        /// Deserializes the stream.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="stream">Stream.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeStream<T>(Stream stream)
        {
            using (TextReader textReader = new StreamReader(stream)) {
                return DeserializeStream<T>(textReader);
            }
        }

        /// <summary>
        /// Deserializes the stream.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="textReader">Text reader.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeStream<T>(TextReader textReader)
        {
            return DeserializeStream<T>(textReader, new JsonSerializerSettings());
        }

        /// <summary>
        /// Deserializes the stream.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="textReader">Text reader.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeStream<T>(TextReader textReader, JsonSerializerSettings serializerSettings)
        {
            return (T)DeserializeStream(textReader, typeof(T), serializerSettings);
        }

        /// <summary>
        /// Deserializes the string.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="json">Json.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeString<T>(string json)
        {
            return DeserializeString<T>(json, new JsonSerializerSettings());
        }

        /// <summary>
        /// Deserializes the string.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="json">Json.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        /// <typeparam name="T">The type parameter.</typeparam>
        public static T DeserializeString<T>(string json, JsonSerializerSettings serializerSettings)
        {
            StringReader reader = new StringReader(json);
            return DeserializeStream<T>(reader, serializerSettings);
        }

        /// <summary>
        /// Deserializes the stream.
        /// </summary>
        /// <returns>The deserialized object.</returns>
        /// <param name="textReader">Text reader.</param>
        /// <param name="targetType">Target type.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        private static object DeserializeStream(TextReader textReader, Type targetType, JsonSerializerSettings serializerSettings)
        {
            JsonSerializer serializer = JsonSerializer.Create(serializerSettings);
            try {
                return serializer.Deserialize(textReader, targetType);
            } catch (Exception e) {
                throw e;
            } finally {
                textReader.Close();
            }
        }

        /// <summary>
        /// Serializes the file.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="o">The object to serialize.</param>
        public static void SerializeFile(string fileName, object o)
        {
            SerializeFile(fileName, o, new JsonSerializerSettings());
        }

        /// <summary>
        /// Serializes the file.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="o">The object to serialize.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        public static void SerializeFile(string fileName, object o, JsonSerializerSettings serializerSettings)
        {
            FileStream writeStream = new FileStream(fileName, FileMode.Create);
            TextWriter textWriter = new StreamWriter(writeStream);
            SerializeStream(textWriter, o, serializerSettings);
            textWriter.Close();
        }

        /// <summary>
        /// Serializes the stream.
        /// </summary>
        /// <param name="textWriter">Text writer.</param>
        /// <param name="o">The object to serialize.</param>
        public static void SerializeStream(TextWriter textWriter, object o)
        {
            SerializeStream(textWriter, o, new JsonSerializerSettings());
        }

        /// <summary>
        /// Serializes the stream.
        /// </summary>
        /// <param name="textWriter">Text writer.</param>
        /// <param name="o">The object to serialize.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        public static void SerializeStream(TextWriter textWriter, object o, JsonSerializerSettings serializerSettings)
        {
            JsonTextWriter jsonWriter = new JsonTextWriter(textWriter);
            jsonWriter.Formatting = Formatting.Indented;

            JsonSerializer serializer = JsonSerializer.Create(serializerSettings);

            try {
                serializer.Serialize(jsonWriter, o);
            } catch (Exception e) {
                throw e;
            }
        }

        /// <summary>
        /// Serializes the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="o">Object to serialize.</param>
        public static string SerializeString(object o)
        {
            return SerializeString(o, new JsonSerializerSettings());
        }

        /// <summary>
        /// Serializes the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="o">Object to serialize.</param>
        /// <param name="serializerSettings">Serializer settings.</param>
        public static string SerializeString(object o, JsonSerializerSettings serializerSettings)
        {
            StringWriter writer = new StringWriter();
            SerializeStream(writer, o, serializerSettings);
            return writer.ToString();
        }
    }
}
#endif
