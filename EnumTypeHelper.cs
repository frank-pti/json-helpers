#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Helper class for enum types.
    /// </summary>
	public static class EnumTypeHelper
	{
		private static Type GetActivationType<T>(T value, Type baseType)
		{
            Type type = value.GetType();
            MemberInfo[] memberInfo = type.GetMember(value.ToString());
            if (memberInfo.Length == 0) {
                throw new Exception(String.Format("No member with the required value ({0}) found on type {1}", value.ToString(), type));
            }
            TypeAttribute[] attributes = memberInfo[0].GetCustomAttributes(typeof(TypeAttribute), false) as TypeAttribute[];
            if (attributes.Length == 0) {
                throw new Exception(String.Format("The {0} member of the {1} enum has no Type attribute", value.ToString(), type));
            }
			if (!attributes[0].Type.IsSubclassOf(baseType)) {
				throw new Exception(string.Format("The {0} member of the {1} enum has a type attribute that specifies a type that is not a subclass of {2}", value.ToString(), type, baseType));
			}
            return attributes[0].Type;
		}

        /// <summary>
        /// Build a dictonary that maps enum values to a base type.
        /// </summary>
        /// <returns>The types.</returns>
        /// <param name="baseType">Base type of all types specified in the type attributes of the enum values.</param>
        /// <typeparam name="T">The enum type parameter.</typeparam>
		public static IDictionary<T, Type> BuildTypes<T>(Type baseType)
		{
            Dictionary<T, Type> dictionary = new Dictionary<T, Type>();
            T[] values = Enum.GetValues (typeof(T)) as T[];
            foreach (T value in values)
            {
                dictionary.Add (value, GetActivationType<T>(value, baseType));
            }
            return dictionary;
		}
	}
}
#endif
