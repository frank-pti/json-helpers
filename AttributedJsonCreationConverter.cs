#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Specifies a converter for instanciating the correct subclass of type T as specified in enum E using the
    /// <see cref="TypeAttribute"/>.
    /// </summary>
	public class AttributedJsonCreationConverter<T, E>: JsonCreationConverter<T>
	{
		private IDictionary<string, E> enumTypes;
		private IDictionary<E, Type> activationTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Json.AttributedJsonCreationConverter`2"/> class.
        /// </summary>
		public AttributedJsonCreationConverter ()
		{
			enumTypes = EnumDescriptionHelper.BuildDictionary<E>();
			activationTypes = EnumTypeHelper.BuildTypes<E>(typeof(T));
		}

        /// <inheritdoc/>
		protected override T Create (Type objectType, JObject jObject)
		{
			JToken token = jObject["type"];
			string stringValue = token.Value<string>();
			E enumType = enumTypes[stringValue];
			return (T)Activator.CreateInstance(activationTypes[enumType]);
		}
	}
}
#endif
