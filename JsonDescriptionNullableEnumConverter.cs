#if !MICRO_FRAMEWORK
/*
 * A DotNet library containing extensions for Newtonsoft.Json library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;

namespace Frank.Helpers.Json
{
    /// <summary>
    /// Converter for nullable enum descriptions.
    /// </summary>
    public class JsonDescriptionNullableEnumConverter<T>: JsonDescriptionEnumConverter<T>
        where T: struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Helpers.Json.JsonDescriptionNullableEnumConverter`1"/> class.
        /// </summary>
        public JsonDescriptionNullableEnumConverter ()
        {
        }

#region implemented abstract members of Newtonsoft.Json.JsonConverter
        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) {
                writer.WriteNull();
            }
            base.WriteJson (writer, (value as Nullable<T>).Value, serializer);
        }

        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) {
                return null;
            }
            return base.ReadJson (reader, typeof(T), existingValue, serializer);
        }

        /// <inheritdoc/>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Nullable<T>) && typeof(T).IsEnum;
        }
        #endregion
    }
}
#endif
